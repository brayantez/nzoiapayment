<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/validation', [\App\Http\Controllers\MpesaController::class,'validationUrl']);

Route::post('v1/confirmation', [\App\Http\Controllers\MpesaController::class,'confirmationUrl']);

Route::post('v1/registerUrl', [\App\Http\Controllers\MpesaController::class,'registerUrls']);

Route::post('v1/transactionStatus', [\App\Http\Controllers\MpesaController::class,'transactionStatus']);

/**
 *
 * MOBILE  APP APIS
 */
Route::group([
    'prefix' => 'v1',
], function () {
    Route::post('login', [\App\Http\Controllers\MAuthController::class, 'login']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('payments', [\App\Http\Controllers\MAuthController::class, 'getMpesaTransaction']);
        Route::post('payments/{id}', [\App\Http\Controllers\MAuthController::class, 'updateMpesaTransactionStatus']);
    });
});