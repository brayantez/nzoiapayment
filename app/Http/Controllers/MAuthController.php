<?php

namespace App\Http\Controllers;

use App\Models\C2bPayments;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MAuthController extends Controller
{
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response(['message' => 'Invalid Credentials User Not found'], 500);
        }

        $check = !Hash::check($request->password, $user->password);
        if ($check) {
            return response(['message' => 'Invalid Credentials'], 500);
        }

        $accessToken = $user->createToken('authToken')->plainTextToken;
        return response([
            'access_token' => $accessToken,
            'user' => $user,
        ]);
    }
    public function getMpesaTransaction()
    {

        $data = C2bPayments::whereNull('ExtRef')
            ->orderBy('id', 'asc')
            ->get();

        return response([
            'data' => $data,
        ]);
    }

    public function updateMpesaTransactionStatus(Request $request, $id)
    {

        $data = C2bPayments::where('id', $id)->first();

        if ($data) {

            $data->update(
                [
                    'ExtRef' => $request['ExtRef'],
                ]
            );
        }

        return true;
    }
}
