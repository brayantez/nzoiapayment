<?php

namespace App\Http\Controllers;


use App\Models\C2bPayments;
use Carbon\Carbon;
use Mpesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class MpesaController extends Controller
{

    public function simulateC2BPayment(Request $request)
    {

        $options = $request->all();


        $validator = Validator::make($options, [
            'msisdn' => 'required',
            'amount' => 'required',
            'billReference' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        } else {

            $simulateResponse = Mpesa::simulateC2B($options['amount'], $options['msisdn'], $options['billReference']);

            return response()->json(['response' => $simulateResponse]);
        }
    }

    public function registerUrls(){

        $registerUrlsResponse = Mpesa::c2bRegisterUrls();

        return $registerUrlsResponse;
    }

    public function validationUrl(Request $request){


        $options = $request->all();


        $validator = Validator::make($options, [
            'TransactionType' => 'required',
            'TransID' => 'required',
            'TransTime' => 'required',
            'TransAmount' => 'required',
            'BusinessShortCode' => 'required',
            'BillRefNumber' => 'required',
//            'InvoiceNumber' => 'required',
            'OrgAccountBalance' => 'required',
//            'ThirdPartyTransID' => 'required',
            'MSISDN' => 'required',
            'FirstName' => 'required',
            'MiddleName' => 'required',
//            'LastName' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        } else {
            Log::info("GET Incoming Request: validation URL");
            Log::info("=====================================================================================================");
            Log::info($request->getContent());
            Log::info("=====================================================================================================");
            $response = Http::post('http://197.248.96.70:86/nzoia/paymentengine/validate.php',$options);
            Log::info("GET Validation Response");
            Log::info("=====================================================================================================");
            Log::info($response);
            Log::info("=====================================================================================================");
            return $response->json();
        }
    }

    public function confirmationUrl(Request $request){
        $options = $request->all();


        $validator = Validator::make($options, [
            'TransactionType' => 'required',
            'TransID' => 'required',
            'TransTime' => 'required',
            'TransAmount' => 'required',
            'BusinessShortCode' => 'required',
            'BillRefNumber' => 'required',
//            'InvoiceNumber' => 'required',
            'OrgAccountBalance' => 'required',
//            'ThirdPartyTransID' => 'required',
            'MSISDN' => 'required',
            'FirstName' => 'required',
            'MiddleName' => 'required',
//            'LastName' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        } else {
            Log::info("GET Incoming Request: confirmation URL");
            Log::info($request->getContent());


            $year = substr($request->TransTime, 0, 4);
            $month = substr($request->TransTime, 4, 2);
            $day = substr($request->TransTime, 6, 2);
            $hour = substr($request->TransTime, 8, 2);
            $minute = substr($request->TransTime, 10, 2);
            $second = substr($request->TransTime, 12, 2);
            $transaction_date = Carbon::create($year, $month, $day, $hour, $minute, $second, 'Africa/Nairobi');

           $params = [
                'TransactionType' => $request->TransactionType,
                'TransID' => $request->TransID,
                'TransTime' => Carbon::parse($transaction_date)->toDateTimeString(),
                'TransAmount' => $request->TransAmount,
//                'BusinessShortCode' => $request->BusinessShortCode,
                'BusinessShortCode' => "MPESA",
                'BillRefNumber' => $request->BillRefNumber,
                'InvoiceNumber' => $request->InvoiceNumber,
                'OrgAccountBalance' => $request->OrgAccountBalance,
//                'ThirdPartyTransID' => $request->ThirdPartyTransID,
               'ThirdPartyTransID' => "PAYBILL",
                'MSISDN' => $request->MSISDN,
                'FirstName' => $request->FirstName,
                'MiddleName' => $request->MiddleName,
                'LastName' => $request->LastName,
                'status' => "5"
            ];

            C2bPayments::create($params);

            Log::info("=====================================================================================================");
            $response = Http::post('http://197.248.96.70:86/nzoia/paymentengine/c2bpayments.php',$params);
            Log::info("GET Confirmation Response");
            Log::info("=====================================================================================================");
            Log::info($response);
            Log::info("=====================================================================================================");
            return $response->json();
        }
    }

    public function transactionStatus(Request $request){
        $options = $request->all();

        $validator = Validator::make($options, [
            'transactionId' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        } else {
            $transactionStatus = Mpesa::status_request($options['transactionId']);
            return response()->json(['success' => $transactionStatus]);
        }
    }

}
