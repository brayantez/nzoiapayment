<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class C2bPayments extends Model
{
    use HasFactory;

    protected $fillable = [
        'TransactionType', 'TransID', 'TransAmount',  'TransTime', 'MSISDN', 'BusinessShortCode', 'OrgAccountBalance', 'ThirdPartyTransID', 'InvoiceNumber','FirstName','MiddleName','LastName','BillRefNumber','status','ExtRef'
    ];
}